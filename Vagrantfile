VAGRANT_PLUGINS = %w(
  vagrant-cachier
  vagrant-hostmanager
).each do |plugin|
  raise "plugin #{plugin} required!" unless Vagrant.has_plugin?(plugin)
end

Vagrant.require_version '>= 1.8.4'

Vagrant.configure(2) do |config|
  config.cache.scope = :box

  config.hostmanager.enabled = true
  config.hostmanager.enable_offline = true
  config.vm.define 'builder' do |config|
    config.vm.box = 'ubuntu/xenial64'
    config.vm.box_check_update = false
    config.vm.hostname = 'builder'

    config.vm.network 'private_network', ip: '10.11.11.10'
    config.vm.provider 'virtualbox' do |v|
      v.memory = 1024
      v.cpus = 2
    end
    config.vm.provision 'common', type: 'shell', path: 'provision_scripts/common.sh'
    config.vm.provision 'builder', type: 'shell', path: 'provision_scripts/builder.sh'
  end

  config.vm.define 'runner' do |config|
    config.vm.box = 'ubuntu/xenial64'
    config.vm.box_check_update = false
    config.vm.hostname = 'runner'

    config.vm.network 'private_network', ip: '10.11.11.11'
    config.vm.provider 'virtualbox' do |v|
      v.memory = 1024
      v.cpus = 2
    end
    config.vm.provision 'common', type: 'shell', path: 'provision_scripts/common.sh'
    config.vm.provision 'service', type: 'shell', path: 'provision_scripts/service.sh'
  end
end
