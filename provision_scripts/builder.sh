#!/bin/bash -e
FILE='/home/ubuntu/.registry_run'
  if [  -f $FILE ];
  then
    echo "Already have registry"
  else
    cd /vagrant/registry
    docker run -d -p 5000:5000 --restart=always --name registry   -v "$(pwd)"/certs:/certs   -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt   -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key   registry:2
    touch $FILE
  fi

  FILE='/home/ubuntu/.build_tools'
  if [  -f $FILE ];
  then
    echo "Already have build tools"
  else
    apt-get -y install golang
    touch $FILE
  fi

cp /vagrant/provision_scripts/.bash_profile /home/ubuntu/
chown ubuntu:ubuntu /home/ubuntu/.bash_profile
