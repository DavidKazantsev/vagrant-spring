#!/bin/bash -e
FILE='/home/ubuntu/.docker_installed'

if [ -f $FILE ];
then
  echo "Already installed"
else
  echo "Installing docker"
  apt-get -y install docker.io
  usermod -a -G docker ubuntu
  touch $FILE
fi

FILE='/home/ubuntu/.registry_cert'
REGHOST=builder
if [ -f $FILE ];
then
  echo "Already have run registry cert"
else
  mkdir -p /etc/docker/certs.d/"${REGHOST}":5000
  cp /vagrant/registry/certs/domain.crt  /etc/docker/certs.d/"${REGHOST}":5000/ca.crt
  systemctl restart docker
  touch $FILE
fi
