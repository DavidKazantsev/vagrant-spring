# README #

Тест для получения контейрезированного в docker приложения java на основе spring.

В конечном образе отсутствуют maven и jdk.

# Сборка и запуск

Собрать и запустить виртуальные машины
```
vagrant up
```

Проверить состояние
```
vagrant status
```

Войти на "сервер сборки"
```
vagrant ssh builder
```

Собрать rocker:
```
mkdir $HOME/go
GO15VENDOREXPERIMENT=1 go get github.com/grammarly/rocker
```
Перейти в директорию `/vagrant/app` и собрать и отправить образ в registry.
```
rocker --verbose build --push
```


Зайти на "ноду" 1 или 2
```
vagrant ssh test-kaz-1

```
Надо стать рутом
```
sudo -i
```

Запустить приложение с тегом production.
```
/vagrant/scripts/run.sh production
```
Просмотреть статус приложения
```
systemctl status docker-spring-app.service
```

Логи в `journalctl`


# Изменение приложения:

Вернуться на `builder`. Внести изменения в файл app/pom.xml или в файлы в каталоге app/src/main/java/hello/. Для изменения тега отредактировать Rockerfile.

Запустить сборку по новой, за счёт кеширования она должна пройти быстрее.
```
rocker --verbose build --push
```


На `runner` снова запустить приложение с тегом production.
```
/vagrant/scripts/run.sh production
```



Так как кроме тега `production` ещё есть и другой тег. То можно запустить и его, например 1.
```
/vagrant/scripts/run.sh 1
```

# Удаление виртуальных машин
vagrant destroy -f


### Известные недостатки
* Ключ от docker registry в репозитории
* необходимо следить за docker registry
* Возможны ненужные выкатка и перезапуск текущей версии
* в java проекте отсутствую тесты
* приложение работает под root'ом

### Ссылки
* https://www.vagrantup.com/
* https://www.virtualbox.org/
* https://github.com/grammarly/rocker
