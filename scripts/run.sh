#!/bin/bash -e

REQ_VER=$1
docker pull builder:5000/spring-app:"${REQ_VER}"

docker tag "$(docker images --format "{{.Repository}}:{{.Tag}} {{.ID}}" | grep "builder:5000/spring-app:${REQ_VER} " | awk '{print $2}')" builder:5000/spring-app:production

systemctl enable docker-spring-app

systemctl stop docker-spring-app.service

while read -r image_id; do
  docker stop "${image_id}"
  docker rm "${image_id}"
done< <(docker ps -a --filter=name=spring-dapp --format "{{.ID}}")

systemctl start docker-spring-app.service
